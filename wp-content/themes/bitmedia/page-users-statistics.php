<?php
get_header();
global $wpdb;
?>
<main id="primary" class="site-main users-pages">
    <?php
    $test = new UsersTable();
    $test->table_generate(); ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-site">
                   <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
                </div>
            </div>
        </div>
        <div class="users-table-staff">
            <div class="row justify-content-between">
                <div class="col-lg-7 col-md-6">
                    <div class="users-title">
                        <p>Users statistics</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-center">
                    Show:
                    <select class="form-control" name="state" id="maxRows">
                        <option value="5000">all users</option>
                        <option value="50">50 users</option>
                        <option value="70">70 users</option>
                        <option value="100">100 users</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="custom-table table-responsive">
                    <table class="table" id="example">
                        <tr class="table-head">
                            <td>Id</td>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Email</td>
                            <td>Gender</td>
                            <td>IP address</td>
                            <td>Total clicks</td>
                            <td>Total page views</td>
                        </tr>
                        <?php
                        foreach ($test->table_generate() as $table){
                            echo'<tr class="table-body" data-href="'.home_url().'/users-statistics/user/'.$table['user_id'].'">
                                            <td>'.$table['user_id'].'</td>
                                            <td>'.$table['first_name'].'</td>
                                            <td>'.$table['last_name'].'</td>
                                            <td>'.$table['email'].'</td>
                                            <td>'.$table['gender'].'</td>
                                            <td>'.$table['ip_address'].'</td>
                                            <td>'.$table['total_views'].'</td>
                                            <td>'.$table['total_clicks'].'</td>
                                        </tr>';
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="pagination-container">
                    <nav>
                        <ul class="pagination">
                            <li data-page="prev" class="prev">
                                <span> <i class="arrow left"></i> <span class="sr-only">(current)</span></span>
                            </li>
                            <li data-page="next" id="prev" class="next">
                                <span> <i class="arrow right"></i> <span class="sr-only">(current)</span></span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>


    </div>

</main><!-- #main -->
<?php
get_footer();