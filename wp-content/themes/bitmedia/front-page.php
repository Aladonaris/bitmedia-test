<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bitmedia
 */

get_header();
?>
<main id="primary" class="site-main">
    <div class="site-mobile">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-7">
                    <div class="template-title">
                        <span style="font-weight: bold">Brainstorming</span> for desired perfect Usability
                    </div>
                    <div class="template-description">
                        Our design projects are fresh and simple and will benefit your business greatly. Learn more about our work!
                    </div>
                    <div class="template-button">
                        <a href="/users-statistics">View Stats</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 offset-lg-2 d-flex justify-content-center">
                    <img src="<?php echo get_template_directory_uri()?>/img/iPhoneX.png">
                </div>
            </div>
        </div>
    </div>
    <div class="benefits">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5">
                    <div class="benefits-title">
                        Why <span style="font-weight: bold"> small business owners love</span> AppCo?
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="benefits-description">
                        Our design projects are fresh and simple and will benefit your business greatly. Learn more about our work!
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="benefit">
                        <div class="benefit-image">
                            <img src="<?php echo get_template_directory_uri()?>/img/cleanDesign.png">
                        </div>
                        <div class="benefit-name">
                            Clean Design
                        </div>
                        <div class="benefit-description">
                            Increase sales by showing true dynamics of your website.
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="benefit">
                        <div class="benefit-image">
                            <img src="<?php echo get_template_directory_uri()?>/img/secure.png">
                        </div>
                        <div class="benefit-name">
                            Secure Data
                        </div>
                        <div class="benefit-description">
                            Build your online store’s trust using Social Proof & Urgency.
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="benefit">
                        <div class="benefit-image">
                            <img src="<?php echo get_template_directory_uri()?>/img/retina.png">
                        </div>
                        <div class="benefit-name">
                            Retina Ready
                        </div>
                        <div class="benefit-description">
                            Realize importance of social proof in customer’s purchase decision.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="managing">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="manage-title">
                        Start Managing your apps business, more faster
                    </div>
                    <div class="manage-description">
                        Objectively deliver professional value with diverse web-readiness. Collaboratively transition wireless customer service without goal-oriented catalysts for change. Collaboratively.
                    </div>
                    <div class="manage-link">
                        <a href="#">Learn more</a>
                    </div>
                </div>
                <div class="col-md-7 managing-width d-flex align-items-center">
                    <div class="managing-image">
                        <img src="<?php echo get_template_directory_uri()?>/img/macbook.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pricing-packages">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-12">
                    <div class="pricing-title">
                        <p style="font-weight: bold">Afforadble Pricing and Packages</p> choose your best one
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-7 col-12">
                    <div class="pricing-description">
                        <p>Monotonectally grow strategic process improvements vis-a-vis integrated resources.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="price-block">
                        <div class="price-name">
                            Basic
                        </div>
                        <div class="price-image">
                            <img src="<?php echo get_template_directory_uri()?>/img/undraw_online_test_gba72.png">
                        </div>
                        <div class="price-cost">
                            $29
                        </div>
                        <div class="price-features">
                            <ul>
                                <li>Push Notifications</li>
                                <li>Data Transfer</li>
                                <li>SQL Database</li>
                                <li>Search & SEO Analytics</li>
                                <li>24/7 Phone Support</li>
                                <li>2 months technical support</li>
                                <li>2+ profitable keyword</li>
                            </ul>
                        </div>
                        <div class="price-button">
                            <button>Purchase now</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="price-block">
                        <div class="price-name">
                            Standart
                        </div>
                        <div class="price-image">
                            <img src="<?php echo get_template_directory_uri()?>/img/undraw_file_sync_ot381.png">
                        </div>
                        <div class="price-cost">
                            $149
                        </div>
                        <div class="price-features">
                            <ul>
                                <li>Push Notifications</li>
                                <li>Data Transfer</li>
                                <li>SQL Database</li>
                                <li>Search & SEO Analytics</li>
                                <li>24/7 Phone Support</li>
                                <li>2 months technical support</li>
                                <li>2+ profitable keyword</li>
                            </ul>
                        </div>
                        <div class="price-button">
                            <button>Purchase now</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="price-block">
                        <div class="price-name">
                            Unlimited
                        </div>
                        <div class="price-image">
                            <img src="<?php echo get_template_directory_uri()?>/img/undraw_quiz_nlyh1.png">
                        </div>
                        <div class="price-cost">
                            $39
                        </div>
                        <div class="price-features">
                            <ul>
                                <li>Push Notifications</li>
                                <li>Data Transfer</li>
                                <li>SQL Database</li>
                                <li>Search & SEO Analytics</li>
                                <li>24/7 Phone Support</li>
                                <li>2 months technical support</li>
                                <li>2+ profitable keyword</li>
                            </ul>
                        </div>
                        <div class="price-button">
                            <button>Purchase now</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="need-more">
                        If you need custom services or Need more? <a href="#">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main><!-- #main -->
<?php
get_footer('main');