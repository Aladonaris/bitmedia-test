<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bitmedia
 */

?>

	<footer id="colophon" class="site-footer-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-design d-flex align-items-center justify-content-between">
                        <a href="<?php home_url();?>" class="footer-app">AppCo</a>
                        <div class="rights">
                            All rights reserved by ThemeTags
                        </div>
                        <div class="copyrights">
                            Copyrights © 2019
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
