<?php
/*
 * Plugin Name: bitmedia test
 * Plugin URI: https://misha.blog/wordpress/plugin
 * Description: this is my best plugin
 * Version: 1.0
 * Author: Vlad Kuprikov
 * License: GPLv2 or later
 */

class UsersTable
{
    private $franchise_mapping;
    // Receives string in CSV format

//    public function initialize()
//    {
//        $json = file_get_contents(dirname(__FILE__) . '/files/users.json');
//        $array = json_decode($json, true);
//        return $array;
//    }
//
//    public function information(){
//        $json = file_get_contents(dirname(__FILE__) . '/files/users_statistic.json');
//        $array = json_decode($json, true);
//        return $array;
//    }

    public function table_generate(){
        global $wpdb;

        $json_users = file_get_contents(dirname(__FILE__) . '/files/users.json');
        $array_users = json_decode($json_users, true);


        $json_statistic = file_get_contents(dirname(__FILE__) . '/files/users_statistic.json');
        $array_statistic = json_decode($json_statistic, true);

        $arr = (array) $wpdb->get_results('SELECT user_id,SUM(page_views),SUM(clicks) FROM users_statistic GROUP BY user_id');

        $users_statistics = array();
        $user_sum = array();
        for($i = 0; $i < count($arr); $i++){
            array_push($users_statistics, (array)$arr[$i]);
        }

        //var_dump($users_statistics);

        for($i = 0; $i < count($array_users); $i++){
            foreach ($users_statistics as $information){
                if(substr_count($array_users[$i]['id'], $information['user_id'])){
                    array_push($user_sum, ['user_id' => $array_users[$i]['id'],
                        'first_name' => $array_users[$i]['first_name'],
                        'last_name' => $array_users[$i]['last_name'],
                        'email' => $array_users[$i]['email'],
                        'gender' => $array_users[$i]['gender'],
                        'ip_address' => $array_users[$i]['ip_address'],
                        'total_views' => $information['SUM(page_views)'],
                        'total_clicks' => $information['SUM(clicks)'],
                    ]);
                    break;
                }
            }
        }
        return $user_sum;
    }
}